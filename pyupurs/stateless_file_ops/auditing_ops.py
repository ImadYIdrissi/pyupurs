import os
import csv
import pandas
import logging
import subprocess

from pyupurs.exceptions.FileIsEmptyError import FileIsEmptyError


def audit_separator(input_file_path: str,
                    output_file_path="faulty_rows.csv",
                    delimiter: str = ",",
                    number_of_columns: int = None,
                    output_to_file=True,
                    rm_out_file: bool = False) -> None or pandas.DataFrame:
    """
    Construct a report of faulty rows in regards to badly delimited data.

    :param input_file_path: path towards the file to audit. It has to be a structured delimited data file, such as csv.
    :param output_file_path: output file of the faulty rows.
    :param delimiter: separator of columns within the data file
    :param number_of_columns: number of known columns in the schema.
        If not specified, the number of columns will be calculated by counting the number of columns per row,
        then taking the number of columns that appears the most.
    :param output_to_file: If set to False, only stdout will display the faulty rows and not write to file output.
    :param rm_out_file: if set to True, will delete the output file of faulty rows.
    :return: A dataframe with the faulty row information.
    """

    # Check file existence
    if not os.path.isfile(input_file_path):
        raise FileNotFoundError

    # Check if file is empty
    if not os.path.getsize(input_file_path) > 0:
        raise FileIsEmptyError(f"The file {input_file_path} is completely empty.")

    with open(input_file_path, "r") as f_reader, open(output_file_path, "w") as f_writer:
        f_read_object = csv.reader(f_reader, delimiter=delimiter)
        f_write_object = csv.writer(f_writer, delimiter="|")
        header = next(f_read_object)
        if not number_of_columns:
            # Estimate of the number of cols. Assuming the number of cols in the header is a valid criterion.
            number_of_columns = len(header)

        for row in f_read_object:
            if len(row) != number_of_columns:
                print(f"FAULTY ROW - {row}")  # TODO : Replace with logging
                if output_to_file:
                    f_write_object.writerow(row)

        if output_to_file and rm_out_file:
            cmd = ["rm", f"{output_file_path}"]
            subprocess.run(cmd)
            print(f"CLEAN UP - Removed file {output_file_path}")  # TODO : Replace with logging


def audit_char_balance(input_file_path: str,
                       output_file_path="faulty_rows.csv",
                       delimiter: str = ",",
                       char: str = '"',
                       output_to_file=True,
                       rm_out_file: bool = False):
    """
    Parse a file to find unbalanced characters, such as parenthesis, brackets, double-quotes, etc..

    :param input_file_path: path towards the file to audit. It has to be a structured delimited data file, such as csv.
    :param output_file_path: output file of the faulty rows.
    :param delimiter: separator of columns within the data file
    :param char: character to audit. [WIP : Only double-quote is supported for now]
    :param output_to_file: If True, will output faulty rows to an external file (output_file_path)
    :param rm_out_file: If True, will remove the external file (output_file_path)
    """
    with open(input_file_path, "r") as readfile, open(output_file_path, "w") as outfile:
        for line in readfile:
            if line.count(char) % 2 != 0:
                outfile.write(line)
