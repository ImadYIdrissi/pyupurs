import csv


def count_cols_per_row(file_path: str, delimiter=",") -> dict:
    """
    In a structured delimiter-separated file, count the number of columns per row.
    Note : This function operates without loading the file in memory.

    :param file_path: Path of the file to process.
    :param delimiter: Delimiter used within the file
    :return: A dictionary for which the keys are the index of the row, and the value are its number of columns.
    """
    # TODO : Unit test this.
    # TODO : This shit is not stateless, it gives MemoryErrors... Use a csv.writer instead.
    with open(file_path, "r") as file:
        f_obj = csv.reader(file, delimiter=delimiter)
        col_count = [len(f) for f in f_obj]
        dict_col_count = {i: list_v for i, list_v in enumerate(col_count)}

        return dict_col_count


def count_rows(file_path: str, delimiter="|", count_header=True) -> int:
    """
    Count the number of rows in a structured separated file.
    Note : This function operates without loading the file in memory.

    :param file_path:
    :param delimiter:
    :param count_header:
    :return: The number of rows.
    """
    # TODO : Unit test this
    with open(file_path, "r") as file:
        f_obj = csv.reader(file, delimiter=delimiter)
        row_count = sum(1 for row in f_obj)
        if not count_header:
            row_count -= 1

        return row_count


def build_output_f_path(file_path: str, suffix: str = "output") -> str:
    """
    Build an enriched a file_path with a suffix. The purpose is to output in the same directory as the input file.
    Example : dirA/dirB/file_name.extension -> dirA/dirB/file_name_suffix.extension

    :param file_path: File path to enrich
    :param suffix: the appended term to the original file path.
    :return: enriched file_path.
    """
    output_path = file_path.split("/")[:-1]
    f_name = file_path.split("/")[-1]
    f_name_radical = f_name.split(".")[0]
    output_path.append(f_name_radical)
    f_ext = f_name.split(".")[-1]
    output_file = f'{"/".join(output_path)}_{suffix}.{f_ext}'

    return output_file

