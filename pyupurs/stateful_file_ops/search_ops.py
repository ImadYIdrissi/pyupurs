import re
import json
import logging
from copy import copy

from pyupurs.stateless_file_ops.transversal_ops import count_rows, build_output_f_path


def search_file_and_match(file_to_search: str, terms: list,
                          lead_rows_to_ignore: int = 1, tail_rows_to_ignore: int = 1,
                          suffix: str = "output", list_of_dicts=True, search_field="name") -> None:
    """
    Search a file, find the rows that contain an element of the terms to search for.
    A new file is created with the relevant rows which contain elements of the list "terms".

    LIMITATIONS:
    - The input file needs to be formatted in a way where 1 row is one complete information to keep.
    - Keep the leading and tailing characters of the file ('{', '}' or '[',']') in the first and last rows;
                separated from the useful content of the file.
    -> Example a json may contain several depths of information. Keep all the depths in-line with the first key
        { <-- Leading '{' to be separated
            "key1":{"subkey1":"subvalue1","subkey2":"subvalue2"}    | Row 1 of the content of the file
            "key2":{"subkey1":"subvalue1","subkey2":"subvalue2"}    | Row 2 of the content of the file
            "key3":{"subkey1":"subvalue1","subkey2":"subvalue2"}    | Row 3 of the content of the file
        } <-- Tailing '}" to be separated
                # TODO : Think of a way to enforce this? perhaps for JSONS only
    :param file_to_search: Input file to parse
    :param terms: The list of terms to search for
    :param lead_rows_to_ignore: the number of leading rows to ignore during search, and replicate in output file
    :param tail_rows_to_ignore: the number of tailing rows to ignore during search, and replicate in output file
    :param suffix: to append to original file name to create the new output file
    :param list_of_dicts: If the input file is a list of dictionaries (Like a BQ schema)
    :param search_field: Field comparison used if list_of_dicts is set to True

    """
    # TODO : unit-test this

    output_file = build_output_f_path(file_to_search, suffix=suffix)

    with open(file_to_search, 'r') as f_r, open(output_file, 'w') as f_w:
        to_parse = {}
        count = 0
        lr = copy(lead_rows_to_ignore)
        tr = copy(tail_rows_to_ignore)
        nb_rows_content = count_rows(file_to_search) - lr - tr
        while True:
            f_r_row = f_r.readline()

            # EOF condition
            if not f_r_row:
                break
            # Put row in dict for O(1) look-up search.
            to_parse[f_r_row] = None
            if count < lr:
                print(f"WRITE - Leading row in {output_file}")
                f_w.write(f_r_row)
            elif lead_rows_to_ignore <= count < lead_rows_to_ignore + nb_rows_content:
                for term in terms:
                    original_row = f_r_row
                    comparison_condition = term in original_row

                    if list_of_dicts:
                        f_r_row_dup = copy(re.sub(r',\n', r'\n', f_r_row))
                        dict_row = json.loads(f_r_row_dup)
                        original_row = dict_row[search_field]
                        comparison_condition = term == original_row

                    if comparison_condition:
                        f_w.write(f_r_row)
                        print_f_row = re.sub(r'\n', '', f_r_row)
                        print(f"MATCHED - {term} matched in \"{print_f_row}\"")
                        print(f"WRITE - row in {output_file}.")
            elif count >= lead_rows_to_ignore + nb_rows_content:
                print(f"WRITE - Tail row in {output_file}")
                f_w.write(f_r_row)
            count += 1


if __name__ == "__main__":
    terms_smart_things = ["MASTER_ID",
                          "GUID",
                          "ORIGIN",
                          "CONTACT_KEY",
                          "COUNTRY",
                          "PRICE_KEY",
                          "ST_JN_YN_FLG",
                          "ST_JN_DT",
                          "ST_TOTAL_DVC_CNT",
                          "ST_IM_DVC_CNT",
                          "ST_IM_MB_DVC_CNT",
                          "ST_IM_TB_DVC_CNT",
                          "ST_IM_GEAR_DVC_CNT",
                          "ST_MNFC_LO_MB_DVC_CNT",
                          "ST_MNFC_LO_TB_DVC_CNT",
                          "ST_MNFC_GL_MB_DVC_CNT",
                          "ST_MNFC_CBRAND_DVC_CNT",
                          "ST_VD_TV_DVC_CNT",
                          "ST_CE_AIRCON_DVC_CNT",
                          "ST_CE_REF_DVC_CNT",
                          "ST_CE_WASHER_DVC_CNT",
                          "ST_CE_DRYER_DVC_CNT",
                          "ST_CE_AIRPUR_DVC_CNT",
                          "ST_CE_AUDIO_DVC_CNT",
                          "ST_CE_ROBOTCL_DVC_CNT",
                          "ST_CE_OVEN_DVC_CNT",
                          "ST_CE_STEAM_DVC_CNT",
                          "ST_CE_COOKTOP_DVC_CNT",
                          "ST_CE_DISHWASHER_DVC_CNT",
                          "ST_LST_LO_MB_CRT_DT",
                          "ST_LST_LO_TB_CRT_DT",
                          "ST_LST_GL_MB_CRT_DT",
                          "ST_LST_CBRAND_CRT_DT",
                          "AEDAT",
                          "INSERT_DATEDAY_KEY",
                          "INSERT_FULLDATE",
                          "UPDATE_DATEDAY_KEY",
                          "UPDATE_DAYTIME_KEY",
                          "UPDATE_FULLDATE"]

    terms_members = ["MASTER_ID",
                     "GUID",
                     "ORIGIN",
                     "COUNTRY",
                     "PRICE_KEY",
                     "CNT_REPORT_6M",
                     "CNT_REPORT_1M",
                     "LAST_REPORT_DVC_NM",
                     "LAST_REPORT_DT",
                     "CNT_POST_1M",
                     "CNT_PUSH_CAMPAIN_6M",
                     "LAST_CLICK_CAMPAIN_NM",
                     "LAST_CLICK_CAMPAIN_DT",
                     "LOYAL_COUPON_ALL_CNT",
                     "LOYAL_COUPON_USED_CNT",
                     "LOYAL_COUPON_VALID_CNT",
                     "LOYAL_COUPON_ALL_CNT_3M",
                     "LOYAL_COUPON_USED_CNT_3M",
                     "LOYAL_COUPON_VALID_CNT_3M",
                     "LOYAL_COUPON_LST_CRT_DT",
                     "LOYAL_COUPON_LST_CRT_TERM",
                     "AEDAT",
                     "INSERT_DATEDAY_KEY",
                     "INSERT_FULLDATE",
                     "UPDATE_DATEDAY_KEY",
                     "UPDATE_DAYTIME_KEY",
                     "UPDATE_FULLDATE"]

    terms_samsung_account = ["MASTER_ID",
                             "GUID",
                             "ORIGIN",
                             "COUNTRY",
                             "AGE",
                             "JOIN_YEAR",
                             "DEV_COUNT_P",
                             "DEV_COUNT_T",
                             "DEV_COUNT_V",
                             "FLAGSHIP_DVC_CNT",
                             "FLAG_S_DVC_CNT",
                             "FLAG_N_DVC_CNT",
                             "MAX_PRICE",
                             "AVG_PRICE",
                             "MIN_ADOPT_MM",
                             "CHANGE_TERM_P",
                             "LAST_DVC_GP_NM",
                             "LASTBEF_DVC_GP_NM",
                             "HOLD_TERM_P",
                             "SHEALTH_FLG",
                             "SPAY_FLG",
                             "MEMBERS_FLG",
                             "SHEALTH_DT",
                             "SPAY_DT",
                             "MEMBERS_DT",
                             "APPS_DT",
                             "THEME_DT",
                             "EMAIL_FLG",
                             "RCNT_MNS3_SVC_VST_NTS",
                             "RCNT_MNS3_SVC_VST_DYS",
                             "RCNT_MNS3_CTNT_DOWN_CNT",
                             "MKT_DVC_GP_NM",
                             "MKT_DVC_SERIES_NM",
                             "MKT_DVC_IMEI_ADDR",
                             "ACTIVE_FLG",
                             "SP_FLAGSHIP_CNT",
                             "SP_DVC_CNT",
                             "SP_NOTE7_CNT",
                             "INACTIVE_TERM",
                             "PRICE_KEY",
                             "SEG_NAME",
                             "SEG_VAL_I1",
                             "SEG_VAL_I2",
                             "SEG_VAL_I3",
                             "SEG_VAL_I4",
                             "SEG_VAL_I5",
                             "SEG_VAL_I6",
                             "SEG_VAL_I7",
                             "SEG_VAL_I8",
                             "SEG_VAL_I9",
                             "SEG_VAL_I10",
                             "SEG_VAL_I11",
                             "SEG_VAL_T1",
                             "SEG_VAL_T2",
                             "SEG_VAL_T3",
                             "DVC_TB_CNT_ALL_NEW",
                             "DVC_WR_CNT_ALL",
                             "DVC_WR_CNT_ALL_NEW",
                             "DVC_RF_CNT_ALL",
                             "DVC_MB_CNT_FLAG_S_NEW",
                             "DVC_MB_CNT_FLAG_N_NEW",
                             "DVC_MB_CNT_NFLAG",
                             "DVC_MB_CNT_NFLAG_NEW",
                             "DVC_MB_CNT_NFLAG_A",
                             "DVC_MB_CNT_NFLAG_A_NEW",
                             "DVC_MB_CNT_NFLAG_J",
                             "DVC_MB_CNT_NFLAG_J_NEW",
                             "DVC_MB_CNT_NFLAG_M",
                             "DVC_MB_CNT_NFLAG_M_NEW",
                             "DVC_MB_CNT_NFLAG_OTHERS",
                             "DVC_MB_CNT_NFLAG_OTHERS_NEW",
                             "DVC_MB_CNT_PHABLET",
                             "DVC_MB_CNT_PHABLET_FLAG",
                             "DVC_MB_SCRN_MAX",
                             "DVC_MB_LST_DVC_SRS_NM",
                             "DVC_MB_LST_ACCO_REG_DT",
                             "DVC_MB_LST_GRSP_DT",
                             "DVC_MB_LST_TERM_ADOPT",
                             "DVC_MB_LST_NET_MCC",
                             "DVC_MB_LST_NET_MNC",
                             "DVC_MB_LST_OLD_FLG",
                             "DVC_MB_LST_OWN_CHANGE_DT",
                             "DVC_MB_LST_OWN_CHANGE_GUID",
                             "DVC_MB_LST_BFR_DVC_MDL_ID",
                             "DVC_MB_LST_BFR_DVC_SRS_NM",
                             "DVC_MB_LST_BFR_ACCO_REG_DT",
                             "DVC_MB_LST_BFR_GRSP_DT",
                             "DVC_MB_LST_BFR_NET_MCC",
                             "DVC_MB_LST_BFR_NET_MNC",
                             "DVC_MB_USE_DVC_MDL_ID",
                             "DVC_MB_USE_ACCO_REG_DT",
                             "DVC_MB_USE_DVC_GRSP_DT",
                             "DVC_MB_USE_TERM_HOLD",
                             "DVC_MB_USE_OLD_FLG",
                             "DVC_MB_PUSH_DVC_GP_NM",
                             "DVC_MB_PUSH_DVC_MDL_ID",
                             "DVC_MB_PUSH_DVC_SRS_NM",
                             "DVC_MB_PUSH_IMEI",
                             "DVC_MB_PUSH_ACCO_REG_DT",
                             "DVC_MB_PUSH_DVC_GRSP_DT",
                             "DVC_MB_PUSH_TERM_HOLD",
                             "DVC_MB_PUSH_NET_MCC",
                             "DVC_MB_PUSH_NET_MNC",
                             "DVC_MB_PUSH_OLD_FLG",
                             "DVC_MB_TERM_INACTIVE_DT",
                             "DVC_TB_LST_DVC_GP_NM",
                             "DVC_TB_LST_DVC_MDL_ID",
                             "DVC_TB_LST_ACCO_REG_DT",
                             "DVC_TB_LST_GRSP_DT",
                             "DVC_TB_LST_TERM_HOLD",
                             "DVC_TV_LST_DVC_GP_NM",
                             "DVC_TV_LST_DVC_MDL_ID",
                             "DVC_TV_LST_ACCO_REG_DT",
                             "DVC_TV_LST_TERM_HOLD",
                             "DVC_RF_LST_MDL_ID",
                             "DVC_RF_LST_ACCO_REG_DT",
                             "DVC_RF_LST_TERM_HOLD",
                             "DVC_WR_LST_DVC_GP_NM",
                             "DVC_WR_LST_DVC_MDL_ID",
                             "DVC_WR_LST_ACCO_REG_DT",
                             "DVC_WR_LST_TERM_HOLD",
                             "SVC_LGN_APPS_FLG",
                             "SVC_LGN_STHEME_FLG",
                             "SVC_LGN_STHINGS_FLG",
                             "SVC_LGN_LST_STHINGS_MM",
                             "SVC_LGN_3M_SHEALTH_FLG",
                             "SVC_LGN_3M_SPAY_FLG",
                             "SVC_LGN_3M_SMEMBERS_FLG",
                             "SVC_LGN_3M_APPS_FLG",
                             "SVC_LGN_3M_STHEME_FLG",
                             "SVC_LGN_3M_STHINGS_FLG",
                             "APPS_CNT_DOWN_ALL_3M",
                             "APPS_CNT_DOWN_FREE_3M",
                             "APPS_CNT_DOWN_PAID_3M",
                             "AEDAT",
                             "INSERT_DATEDAY_KEY",
                             "INSERT_FULLDATE",
                             "UPDATE_DATEDAY_KEY",
                             "UPDATE_DAYTIME_KEY",
                             "UPDATE_FULLDATE"]

    terms_sav = ["MASTER_ID",
                 "GUID",
                 "ORIGIN",
                 "COUNTRY",
                 "PRICE_KEY",
                 "CNT_SVC_ALL_6M",
                 "CNT_SVC_IM_6M",
                 "CNT_SVC_VD_6M",
                 "CNT_SVC_CE_6M",
                 "LAST_SVC_IM_NM",
                 "LAST_SVC_IM_DT",
                 "LAST_SVC_VD_NM",
                 "LAST_SVC_VD_DT",
                 "LAST_SVC_CE_NM",
                 "LAST_SVC_CE_DT",
                 "LAST_INQURY_NM",
                 "LAST_INQURY_TYPE",
                 "LAST_INQURY_DT",
                 "LAST_INQURY_SCORE",
                 "AEDAT",
                 "INSERT_DATEDAY_KEY",
                 "INSERT_FULLDATE",
                 "UPDATE_DATEDAY_KEY",
                 "UPDATE_DAYTIME_KEY",
                 "UPDATE_FULLDATE"]

    terms_shop = ["MASTER_ID",
                  "GUID",
                  "ORIGIN",
                  "COUNTRY",
                  "PRICE_KEY",
                  "CNT_BUY_IM_3M",
                  "CNT_BUY_VD_3M",
                  "CNT_BUY_CE_3M",
                  "LAST_BUY_IM_NM",
                  "LAST_BUY_IM_DT",
                  "LAST_BUY_VD_NM",
                  "LAST_BUY_VD_DT",
                  "LAST_BUY_CE_NM",
                  "LAST_BUY_CE_DT",
                  "AEDAT",
                  "INSERT_DATEDAY_KEY",
                  "INSERT_FULLDATE",
                  "UPDATE_DATEDAY_KEY",
                  "UPDATE_DAYTIME_KEY",
                  "UPDATE_FULLDATE"]

    terms_website = ["MASTER_ID",
                     "GUID",
                     "ORIGIN",
                     "COUNTRY",
                     "PRICE_KEY",
                     "CNT_VIEW_IM_1M",
                     "CNT_VIEW_IM_3M",
                     "CNT_VIEW_VD_1M",
                     "CNT_VIEW_VD_3M",
                     "CNT_VIEW_CE_1M",
                     "CNT_VIEW_CE_3M",
                     "AEDAT",
                     "INSERT_DATEDAY_KEY",
                     "INSERT_FULLDATE",
                     "UPDATE_DATEDAY_KEY",
                     "UPDATE_DAYTIME_KEY",
                     "UPDATE_FULLDATE"]

    # Additional table to add to datahub
    term_contact = ["MASTER_ID",
                     "AEDAT",
                     "SHEALTH_FLG",
                     "SPAY_FLG",
                     "MEMBERS_FLG",
                     "SHEALTH_DT",
                     "SPAY_DT",
                     "MEMBERS_DT",
                     "APPS_DT",
                     "THEME_DT",
                     "RCNT_MNS3_SVC_VST_NTS",
                     "RCNT_MNS3_SVC_VST_DYS",
                     "RCNT_MNS3_CTNT_DOWN_CNT",
                     "ACTIVE_FLG",
                     "INACTIVE_TERM",
                     "CNT_SVC_ALL_6M",
                     "CNT_REPORT_6M",
                     "CNT_PUSH_CAMPAIN_6M",
                     "SVC_LGN_APPS_FLG",
                     "SVC_LGN_STHEME_FLG",
                     "SVC_LGN_STHINGS_FLG",
                     "SVC_LGN_LST_STHINGS_MM",
                     "SVC_LGN_3M_SHEALTH_FLG",
                     "SVC_LGN_3M_SPAY_FLG",
                     "SVC_LGN_3M_SMEMBERS_FLG",
                     "SVC_LGN_3M_APPS_FLG",
                     "SVC_LGN_3M_STHEME_FLG",
                     "SVC_LGN_3M_STHINGS_FLG",
                     "APPS_CNT_DOWN_ALL_3M",
                     "APPS_CNT_DOWN_FREE_3M",
                     "APPS_CNT_DOWN_PAID_3M",
                     "ST_JN_YN_FLG",
                     "ST_JN_DT",
                     "ST_TOTAL_DVC_CNT",
                     "LOYAL_COUPON_ALL_CNT"]

    dict_terms = {
        # "SMART_THINGS": terms_smart_things,
        # "SHOP": terms_shop,
        # "MEMBERS": terms_members,
        # "WEBSITE": terms_website,
        # "SAV": terms_sav,
        # "SAMSUNG_ACCOUNT": terms_samsung_account
        "CONTACTS": term_contact
    }

    for suff in dict_terms:
        print(f"{suff}:{len(dict_terms[suff])}")
        search_file_and_match(file_to_search="../../tests/samples/stateless_file_ops/search_ops/USER_PROFILE.json",
                              terms=dict_terms[suff],
                              lead_rows_to_ignore=1, tail_rows_to_ignore=1,
                              suffix=suff)
