# pyupurs

Yet another python library for useful utilities.

- **How do you pronounce pyupurs ?** - "pie-up-yuurs"

# List of modules and their features
## stateless_file_ops
This package assembles modules to process files without loading data into memory.
### auditing_ops.py 
This module assembles functions to audit data integrity.
### transversal_ops.py
This module assembles functions that operate transversely across file operations.